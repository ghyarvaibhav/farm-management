export class EmergencyTypeModel {
  emergencyTypeId: number;
  emergencyTypeName: string;
  activeStatus: number;
}
