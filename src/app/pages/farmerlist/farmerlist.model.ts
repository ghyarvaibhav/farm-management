export class FarmerlistModel {
  cityName: string;
  id: number;
  firstName: string;
  lastName: string;
  cityId: number;
  gender: string;
  phoneNo: string;
  alternatePhoneNo: string;
  userTypeId: number;
  farmerId:number;
}
