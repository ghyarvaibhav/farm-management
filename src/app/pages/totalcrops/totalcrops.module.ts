import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TotalcropsComponent } from './totalcrops.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [TotalcropsComponent],
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild([
      {
        path: '',
        component: TotalcropsComponent,
      },
    ]),
  ],
})

export class TotalcropsModule { }
